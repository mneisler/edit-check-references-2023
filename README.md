# Edit-check-references-2023

The Wikimedia Foundation's Editing team is working on a set of improvements for the visual editor to help new volunteers understand and follow some of the policies necessary to make constructive changes to Wikipedia projects.

The first version of Edit Check (Reference Check) invites users who have added more than 50 new characters to an article namespace to include a reference to the edit they're making if they have not already done so themselves at the time they indicate their intent to save.

This analysis explores the impact of reference check on quality of new content edits newcomers and Junior Contributors make in the main namespace.

You can find more information about features of this tool and project updates on the [project page](https://www.mediawiki.org/wiki/Edit_check?useskin=vector-2022) and the metrics defined for the impact analysis in the [task](https://phabricator.wikimedia.org/T342930).

